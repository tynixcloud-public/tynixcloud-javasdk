package ru.tynixcloud.sdk.exception;
/**
 * Created by frotter, for tynixcloud.
 * @VK: https://vk.com/frotterr
 */
public class TokenHasExpiredException extends Exception {

    @Override
    public String getLocalizedMessage() {
        return "Токен не верный, или истек его срок действия.";
    }

    @Override
    public String getMessage() {
        return "Token not found, maybe token has expired or invalid.";
    }
}
