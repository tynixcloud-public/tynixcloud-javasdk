package ru.tynixcloud.sdk.request.player;

import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import ru.tynixcloud.sdk.TynixSdk;
import ru.tynixcloud.sdk.request.BaseRequest;
import ru.tynixcloud.sdk.utility.HttpParam;
import ru.tynixcloud.sdk.utility.JsonUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
/**
 * Created by frotter, for tynixcloud.
 * @VK: https://vk.com/frotterr
 */
public class PlayerNameRequest extends BaseRequest {
    public PlayerNameRequest(@NonNull TynixSdk sdk) {
        super(sdk, "player/name");
    }

    public GetPlayerNameResponse getPlayerNameResponse(int playerId) throws ExecutionException, InterruptedException, IOException {
        JsonObject response = JsonUtil.parse(get(request, new HttpParam("id", "" + playerId)))
                .getAsJsonObject()

                .get("response")
                .getAsJsonObject();
        String name = response.get("name").getAsString();

        return new GetPlayerNameResponse(playerId, name);
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    public static class GetPlayerNameResponse {

        int id;
        String name;


    }
}
