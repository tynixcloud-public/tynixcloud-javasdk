package ru.tynixcloud.sdk.request.player;

import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import ru.tynixcloud.sdk.TynixSdk;
import ru.tynixcloud.sdk.request.BaseRequest;
import ru.tynixcloud.sdk.utility.HttpParam;
import ru.tynixcloud.sdk.utility.JsonUtil;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
/**
 * Created by frotter, for tynixcloud.
 * @VK: https://vk.com/frotterr
 */
public class PlayerInformationRequest extends BaseRequest {
    public PlayerInformationRequest(@NonNull TynixSdk sdk) {
        super(sdk, "player/information");
    }

    public GetPlayerInformationResponse getPlayerInformationResponse(@NonNull String playerName) throws ExecutionException, InterruptedException, IOException {
        JsonObject response = JsonUtil.parse(get(request, new HttpParam("name", playerName)))
                .getAsJsonObject()

                .get("response")
                .getAsJsonObject();
        int id = response.get("id").getAsInt();
        String name = response.get("name").getAsString();

        int groupId = response.get("groupId").getAsInt();
        int coins = response.get("coins").getAsInt();
        int golds = response.get("golds").getAsInt();
        String typass = response.get("typass").getAsString();
        int level = response.get("level").getAsInt();
        int experience = response.get("experience").getAsInt();
        String locale = response.get("locale").getAsString();
        String version = response.get("version").getAsString();


        return new GetPlayerInformationResponse(id, name, groupId, coins, golds, typass, level, experience, locale, version);
    }

    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @AllArgsConstructor
    public static class GetPlayerInformationResponse {

        int id;
        String name;
        int groupId;
        int coins;
        int golds;
        String typass;
        int level;
        int experience;
        String locale;
        String version;


    }

}
